function [ sp_fail_tab_struct, error_message  ] = ...
    spf_get_excel_range( an_excel_file_name,excel_tab_number)
%SPF_GET_EXCEL_RANGE returns a cell with 1 set of range of data: one for 1 block of data in the given sheet_table.
% Input:  an_excel_file_name  Excel file name (full path) with data to be processed with the std_curve_fit app.
%         excel_tab_number    Excel tab number(BE AWARE THAT EVEN HIDEN TABS ARE PART OF EXCEL NUMBERING:
%                             Warning: Do not hide tabs in excel: It is very misleading.
% Output: a struct with:
%   1 - the list of spot fail found: The letters of the failures
%   4 - A matrix with the map: wo_die_ndx to wafer_die_ndx
% Assumptions:
%   1 - Excel locations: 1 - Word notch in cell: 7-0
%   2 -                  2 - 22x11 matrix with indices and failures (an alpha letter)
%   3 - All excel tabs are identical. The ones we care are called something like: rack_# or rack#
% pipeline:  sp_fail  -->  spf_part_update --> spf_read_spf_excel_file --> spf_get_excel_range
error_message                   = '';
sp_fail_tab_struct             = struct;
chips_per_tab_max              = 22*11;   %jas_hardcoded_excel_file_dependency_rack_size
sp_fail_ndx                    = cell(chips_per_tab_max,1);
sp_fail_tab_struct.sp_fail_ndx = sp_fail_ndx;

% read the whole sheet as a table:
sheet_table= readtable(an_excel_file_name...
    ,'ReadVariableNames',false,'FileType','spreadsheet'...
    ,'Sheet',excel_tab_number  );
%spf_len_cel   = table2cell(sheet_table(2,1)); % second row value is the spf_data_length (only one col)
try
    if (~(strcmpi(char(table2cell(sheet_table(7,15))),'notch')))
        % FOUND KEYWORD Notch WAS not WHERE IT WAS EXPECTED: Grab the real table
        % get out of here:
        error_message = 'ERROR: Bad EXCEL FILE FORMANT: Keyword  notch not found in cell: 7,15';
        return;
    end
    
    % FOUND KEYWORD Notch WERE IT WAS EXPECTED: Grab the real table
    
    wo_ndx_map_table_ref= sheet_table(8:8+21,16:16+10);
    data_size_rows      = size(wo_ndx_map_table_ref,1);
    data_size_cols      = size(wo_ndx_map_table_ref,2);
    
    % SORT ROWS ACCORDING TO ndx_spot:
    ndx_spot_row                = data_size_rows:-1:1;
    ndx_spot_row                = ndx_spot_row'; % make it vertical
    ndx_spot_row_table          = table(ndx_spot_row);
    wo_ndx_map_table_ref        = [ndx_spot_row_table  wo_ndx_map_table_ref];
    wo_ndx_map_table_ref_sort   = sortrows(wo_ndx_map_table_ref,1); % ascending by col_1
    wo_ndx_map_table_ref        = wo_ndx_map_table_ref_sort;
    %wo_ndx_map_table_ref.ndx_spot_row  = ndx_spot_row;
    % get rid of the first column: not needed anymore.
    wo_ndx_map_table_ref        = wo_ndx_map_table_ref(:,2:end);
    wo_ndx_map_table_ref.Properties.VariableNames = { 'col_1' ,'col_2' ,'col_3' ,'col_4' ,'col_5' ,'col_6' ,'col_7' ,'col_8' ,'col_9' ,'col_10' ,'col_11'};
    
    wo_ndx_map_table 	= wo_ndx_map_table_ref;
    % Clean up the table so the parsing is smooth. Brute force .. simple but dirty ...
    for cur_col_ndx =1:1:size(wo_ndx_map_table,2)
        if (isnumeric(wo_ndx_map_table{:,cur_col_ndx}))
            temp = cellstr(num2str(wo_ndx_map_table{:,cur_col_ndx}));
            if (cur_col_ndx == 1)
                wo_ndx_map_table.col_1 = temp;
            end
            if (cur_col_ndx == 2)
                wo_ndx_map_table.col_2 = temp;
            end
            if (cur_col_ndx == 3)
                wo_ndx_map_table.col_3 = temp;
            end
            if (cur_col_ndx == 4)
                wo_ndx_map_table.col_4 = temp;
            end
            if (cur_col_ndx == 5)
                wo_ndx_map_table.col_5 = temp;
            end
            if (cur_col_ndx == 6)
                wo_ndx_map_table.col_6 = temp;
            end
            if (cur_col_ndx == 7)
                wo_ndx_map_table.col_7 = temp;
            end
            if (cur_col_ndx == 8)
                wo_ndx_map_table.col_8 = temp;
            end
            if (cur_col_ndx == 9)
                wo_ndx_map_table.col_9 = temp;
            end
            if (cur_col_ndx == 10)
                wo_ndx_map_table.col_10 = temp;
            end
            if (cur_col_ndx == 11)
                wo_ndx_map_table.col_11 = temp;
            end
        end
    end
    
    % build list of failure modes from the table:
    alpha_num_str       = struct2array(table2struct(wo_ndx_map_table));
    alpha_only_str      = alpha_num_str(isstrprop(alpha_num_str,'alpha'));
    alpha_unique_str    = unique(alpha_only_str);  % i.e eps
    
    len_alpha           = size(alpha_unique_str,2);
    alpha_cnts_list     = zeros(len_alpha,1);
    fail_rows           = cell(chips_per_tab_max,1);
    fail_cols           = cell(chips_per_tab_max,1);
    fail_rack           = cell(chips_per_tab_max,1);
    fail_alpha          = cell(chips_per_tab_max,1);
    cur_failure_ndx=0;
    for cur_row=1:1:data_size_rows
        for cur_col=data_size_cols:-1:1
            % process them from last col to first col: So they come in col ascending order in printout
            
            cur_elem_unk = wo_ndx_map_table_ref{cur_row,cur_col};
            if (iscell(cur_elem_unk))
                cur_elem = char(table2cell((wo_ndx_map_table_ref(cur_row,cur_col))));
            else
                if (isnumeric(cur_elem_unk))
                    cur_elem = num2str(cur_elem_unk);
                else
                    error_message = sprintf(...
                        'Invalid Element: At Location: %-3d, %-3d Element: %-s',cur_row,cur_col,string(cur_elem_cell));
                    return;
                end
            end
            % HAVE A VALID CUR_ELEM of CELL TYPE.
            % orig:  cur_elem = char(table2cell((wo_ndx_map_table_ref(cur_row,cur_col))));
            [~, status] = str2num(cur_elem);
            if (~(status))
                % ELEMENT CONTAINS AND ALPHA CHAR: get rid of it.
                for cur_char_ndx= 1:1:len_alpha
                    cur_char    = alpha_unique_str(cur_char_ndx);
                    alpha_ndx   = strfind(cur_elem,cur_char);
                    if (alpha_ndx)
                        alpha_pos = strfind(alpha_unique_str,cur_elem(alpha_ndx));
                        alpha_cnts_list(alpha_pos) = alpha_cnts_list(alpha_pos)+1;
                        %fprintf('\ncur_row = %-4d, cur_col = %-4d',cur_row,cur_col);
                        wo_ndx_map_table{cur_row,cur_col} = cellstr(cur_elem(1:alpha_ndx-1));
                        %fprintf('\nok');
                        cur_failure_ndx=cur_failure_ndx+1;
                        sp_fail_ndx{cur_failure_ndx} = str2double(cur_elem(1:alpha_ndx-1));
                        fail_rows{cur_failure_ndx}   = cur_row;
                        fail_cols{cur_failure_ndx}   = cur_col;
                        fail_rack{cur_failure_ndx}   = excel_tab_number;
                        fail_alpha{cur_failure_ndx}   = cur_elem;
                        break;
                    end
                end
            end
        end % for each col
    end % for each row
    
    wo_ndx_map_table = cell2matdouble(table2array(wo_ndx_map_table));
    
    % HAVE THE WO_NDX_MAP TABLE AND THE LIST OF FAILURES. Get out
    fprintf('\n Found Failure count:   \t%-4d ',cur_failure_ndx);
    fprintf('\n Found Failure codes:   \t%-s  ',alpha_unique_str);
    for cur_pos = 1:1:len_alpha
        fprintf('\n                        \tCode: %-s:  Count: %-4d  ',alpha_unique_str(cur_pos),alpha_cnts_list(cur_pos));
    end
    % return only what it was found:
    if (cur_failure_ndx)
        sp_fail_ndx = sp_fail_ndx(1:cur_failure_ndx);
        fail_rows = fail_rows(1:cur_failure_ndx);
        fail_cols = fail_cols(1:cur_failure_ndx);
        fail_rack = fail_rack(1:cur_failure_ndx);
        fail_alpha= fail_alpha(1:cur_failure_ndx);
    end
catch
    error_message = sprintf('ERROR: Invalid Excel file format: \n File: %-s \n Tab: %-3d, Fix it and try again'...
        ,an_excel_file_name,excel_tab_number);
    return;
end % catch
% Save results to return:
sp_fail_tab_struct.sp_fail_ndx      = sp_fail_ndx;      % list of failures wafer indices .
sp_fail_tab_struct.fail_rows        = fail_rows;        % spot row
sp_fail_tab_struct.fail_cols        = fail_cols;        % spot col
sp_fail_tab_struct.fail_rack        = fail_rack;        % spot rack
sp_fail_tab_struct.fail_alpha       = fail_alpha;        % spot rack

sp_fail_tab_struct.wo_ndx_map_table = wo_ndx_map_table; % map wo to spot ndx.
sp_fail_tab_struct.failure_codes    = alpha_unique_str;
end % fn spf_get_excel_range

