function [ qc_rpt_table,error_message,error_flag ] = get_qc_rpt_data_from_runcard( input_params )
%GET_QC_RPT_DATA_FROM_RUNCARD  given a QC WORK ORDER, retrieves a table to be used by the qc_report (Automated version)

% function not in use: only for verification because usually the qc_work_order is unknown. 
% see: get_qc_rpt_data_from_runcard_sp_lot_number

%Output:
% 1 - qc_rpt_table:  A table with the following columns:
%
%    mb_lot_number  mb_serial  mb_part_number  carrier_serial  wafer_serial  qc_rack_number  qc_rack_index
%    _____________  _________  ______________  ______________  ____________  ______________  ______________
% 2 - error_message:   if empty: no errors.
% 3 - error_flag:      if 0 no errors. If 1 some errors, ie incomplete data may had been returned.

% Assumptions:
% 1 - The spot failures are logged in RunCard using the SP_FAIL APP. (Ana / Joshua)
% 2 - Sampling plan is performed in a single Work Order at a time.
% 3 - Multiple sampling plans can be performed in a single work order.
% 4 - When there are multiple sampling plans, they must be applied sequentially. SP1, then SP2 etc.
% 5 - The chips from the sampling plan get pick sequentially from the gel pack
% 6 - All the chips from the Sampling Plan go from the gel packs to another gel pack(s):
%     1_ QC gel_pack sp_1
%     2_LSC gel_pack sp_2
% 7 - Two work orders are  generated: QCP0023-QC (positive/negatives) and QCP0023-LSC (parametric: Known patient samples)
%     (The LSC is to be ignored by this function: It is not part of the QC report.Nick confirmed!)
%     sample WO number: WO EWO-0137
% 8 - Only QCP0023-QC  (SP1 I.E. Bin 2) get reported.
%
% 9 - The Virtual QC racks are filled sequentially from rack 1 to the last and each rack has up to 20 chips/ Matchboxes.
% 10- The count of chips of sampling plan 1 (bin 2) must match with the count of the chips in QC gel_packs.
%     This is the only verification (sanity check) that can/MUST be done to detect handling mistakes.
%
% sample call: see: do_get_qc_rpt_data_from_runcard.m 
% [ qc_rpt_table,error_message,error_flag ] = get_qc_rpt_data_from_runcard( input_params )

%error_message  = '';
error_flag      = 0;
qc_rpt_table    = table();
webservice_url  = input_params.webservice_url; % 'http://10.0.2.226/runcard/soap?wsdl';          % for real
createClassFromWsdl(webservice_url);
obj = runcard_wsdl;

% Fetch wo detail:
serial          = '';                           % return all serials: these are matchbox serials
status          = 'COMPLETE';
part_number     = input_params.part_number;
part_revision   = '';                           % any revision is fine
work_order      = input_params.work_order;
op_code         = input_params.op_code;
seq_number      = input_params.seq_number;      % (int)
warehouse_loc   = input_params.warehouse_loc;   % (string)
warehouse_bin   = input_params.warehouse_bin;   % (string)

[response, error_value, error_message]   = fetchInventoryItems(obj, serial, status, part_number, part_revision...
    , work_order,op_code ,  seq_number, warehouse_loc, warehouse_bin);
if (~(isempty(error_message)))
    error_message =  strcat( error_message, '\nError_value = ',num2str(error_value));
    return
end;
if (isempty(response))
    % NO CARRIERS FOUND
    error_message ='ERROR: no_carriers_found';
    return;
end
% WORK ORDER HAVE 1 OR MORE  CARRIERS
response_table 	= struct2table(response);
lot_number   	= char(unique(categorical(response_table.lotnum)));
serial_cnt      = height(response_table);

% Generate empty table with as many rows as carriers found in the WO.
qc_rpt_headers	= {'mb_work_order','mb_lot_number','mb_serial','mb_part_number','qc_rack_number','qc_rack_index','wafer_serial','wafer_lot','wafer_number','die_index'};
data          	= cell(serial_cnt,size(qc_rpt_headers,2));
qc_rpt_table  	= cell2table(data);
qc_rpt_table.Properties.VariableNames = qc_rpt_headers;

% Fill in the mb serial:
qc_rpt_table.mb_serial = response_table.serial;
delimiter              = {'.'}; % DOT IS THE ONLY VALID SEPARATOR BETWEEN PARTS OF THE SERIAL.
qc_rack_size           = 20;    % jas_hardcoded qc_rac_size
cur_qc_rack_number     = 1;
cur_qc_rack_index      = 0;
for cur_mb_serial_ndx = 1:1:serial_cnt
    % Determine rack number and index for current carrier: produces indices: 1 to 20 , 1 to 20 .. to the end
    cur_qc_rack_index     = cur_qc_rack_index + 1;
    if (cur_qc_rack_index > qc_rack_size)
        cur_qc_rack_number= cur_qc_rack_number+1;
        cur_qc_rack_index = 1;
    end
    qc_rpt_table.mb_work_order(cur_mb_serial_ndx)   = {work_order                               }; % this is the WO being processed
    qc_rpt_table.mb_lot_number(cur_mb_serial_ndx)   = {lot_number                               }; % the lot number  of the carrier becomes the lot  number of the mb
    qc_rpt_table.mb_serial(cur_mb_serial_ndx)       =  response_table.serial(cur_mb_serial_ndx)  ; % the serial      of the carrier becomes the serial of the mb
    qc_rpt_table.mb_part_number(cur_mb_serial_ndx)  = {part_number                              };  % the part number of the carrier becomes the part number of the mb
    qc_rpt_table.qc_rack_number(cur_mb_serial_ndx)  = {cur_qc_rack_number                       };
    qc_rpt_table.qc_rack_index(cur_mb_serial_ndx)   = {cur_qc_rack_index                        };
    
    % HAVE MATCH BOX SERIAL: Need to retrieve the wafer info:
    % Ryan please confirm:
    % Having a given Match Box serial  I need to find the wafer info for the die used to build this matchbox.

    % Additional info: If you select one carrier, let's say: B1600002
    % 1 - Step 20 / A320 shows the following data:  Within the BOM Item consumption record:
    %     Part number: 00445  Serial / Lot: P162143.08.0002 From inventory
    
    % Next lines to be confirmed by Ryan:
    insert_chip_into_carrier_step = num2str(seq_number - 10); % 30 -10 --> 20
    [unit_bom_items, error_value, error_message]   = getUnitBOMItems(obj,char(response_table.serial(cur_mb_serial_ndx)),work_order,insert_chip_into_carrier_step);  % 'B160002'
    
    if (~(isempty(error_message)))
        error_message =  strcat(...
            'ERROR_FROM: getUnitBOMItems:', error_message, '\nError_value = ',num2str(error_value),'For serial: ',char(response_table.serial(cur_mb_serial_nd)));
        error_flag =1;
        if (display_flag)
            sprintf('\n%-s',error_message);
        end
        continue
    end;
    if (isempty(unit_bom_items))
        % NO BOM FOUND FOR THIS SERIAL
        error_message = sprintf('ERROR_FROM: getUnitBOMItems no_items_found for serial %-s',char(response_table.serial(cur_mb_serial_ndx)));
        error_flag =1;
        if (display_flag)
            sprintf('\n%-s',error_message);
        end
        continue;
    end
    % Perform Sanity:
    if ( (isfield(unit_bom_items,'partnum')) && (strcmp(unit_bom_items.partnum,'00445') ) )
        % FOUND EXPECTED PART NUMBER: Get the wafer/die information        
        cur_wafer_serial_str                        = split(unit_bom_items.inv_serial,delimiter);
        cur_wafer_lot                               = cur_wafer_serial_str(1);
        cur_wafer_number                            = double(cur_wafer_serial_str(2)); % the second is the wafer number
        cur_die_index                               = double(cur_wafer_serial_str(3)); % the last part (the 3rd) is now the die_index
        
        qc_rpt_table.wafer_serial(cur_mb_serial_ndx)= {unit_bom_items.inv_serial};
        qc_rpt_table.wafer_lot(cur_mb_serial_ndx)   = {cur_wafer_lot            };
        qc_rpt_table.wafer_number(cur_mb_serial_ndx)= {cur_wafer_number         };
        qc_rpt_table.die_index(cur_mb_serial_ndx)   = {cur_die_index            };
    else
        error_message = sprintf('ERROR_FROM: getUnitBOMItems Empty unit_bom_items for serial %-s',char(response_table.serial(cur_mb_serial_ndx)));
        error_flag =1;
        if (display_flag)
            sprintf('\n%-s',error_message);
        end        
    end        
  
end % for each serial

end % fn get_qc_rpt_data_from_runcard

