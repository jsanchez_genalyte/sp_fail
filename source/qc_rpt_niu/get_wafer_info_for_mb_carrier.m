function [ wafer_table,error_message,error_flag  ] = get_wafer_info_for_mb_carrier( obj, mb_carrier_serial_list,mb_work_order,insert_chip_into_carrier_step,display_flag )
%GET_WAFER_INFO_FOR_MB_CARRIER Retrieves wafer_table for a given: a mb_carrier_serial_list and their  mb_work_order
%   Obj is and existing connection object to runcard.

% This function has not beeen tested/used: because what I need is starting from spotting_lot_number and without knowing the mb_work_order

% jas_tbd: use the carrier to go to the inventory and retrieve lot and workorder. 
%          then do the rest of the work as usual (jas_tbd 05/07/2018
%          this funtion is good to be called by dcrc_gen_xml_files app. 

delimiter     	= {'.'}; % DOT IS THE ONLY VALID SEPARATOR BETWEEN PARTS OF THE SERIAL.
serial_cnt   	= size(mb_carrier_serial_list,2);

wafer_headers	= {'mb_serial','wafer_serial','wafer_lot','wafer_number','die_index'};
data          	= cell(serial_cnt,size(wafer_headers,2));
wafer_table  	= cell2table(data);
wafer_table.Properties.VariableNames = wafer_headers;

for cur_mb_serial_ndx = 1:1:serial_cnt
    % PROCESS A single mb serial at a time
    wafer_table.mb_serial(cur_mb_serial_ndx)       =  mb_carrier_serial_list(cur_mb_serial_ndx); % the serial      of the carrier becomes the serial of the mb
    % HAVE MATCH BOX SERIAL: Need to retrieve the wafer info:
    % Ryan please confirm:
    % Having a given Match Box serial  I need to find the wafer info for the die used to build this matchbox.
    
    % Additional info: If you select one carrier, let's say: B1600002
    % 1 - Step 20 / A320 shows the following data:  Within the BOM Item consumption record:
    %     Part number: 00445  Serial / Lot: P162143.08.0002 From inventory
    
    % Next lines to be confirmed by Ryan:

    [unit_bom_items, error_value, error_message]   = getUnitBOMItems(obj,char(mb_carrier_serial_list{cur_mb_serial_ndx}),mb_work_order,insert_chip_into_carrier_step);  % 'B160002'
    
    if (~(isempty(error_message)))
        error_message =  strcat(...
            'ERROR_FROM: getUnitBOMItems:', error_message, '\nError_value = ',num2str(error_value),'For serial: ',char(response_table.serial(cur_mb_serial_nd)));
        error_flag =1;
        if (display_flag)
            sprintf('\n%-s',error_message);
        end
        continue
    end;
    if (isempty(unit_bom_items))
        % NO BOM FOUND FOR THIS SERIAL
        error_message = sprintf('ERROR_FROM: getUnitBOMItems no_items_found for serial %-s',char(response_table.serial(cur_mb_serial_ndx)));
        error_flag =1;
        if (display_flag)
            sprintf('\n%-s',error_message);
        end
        continue;
    end
    % Perform Sanity:
    if ( (isfield(unit_bom_items,'partnum')) && (strcmp(unit_bom_items.partnum,'00445') ) )
        % FOUND EXPECTED PART NUMBER: Get the wafer/die information
        cur_wafer_serial_str                        = split(unit_bom_items.inv_serial,delimiter);
        cur_wafer_lot                               = cur_wafer_serial_str(1);
        cur_wafer_number                            = double(cur_wafer_serial_str(2)); % the second is the wafer number
        cur_die_index                               = double(cur_wafer_serial_str(3)); % the last part (the 3rd) is now the die_index
        
        wafer_table.wafer_serial(cur_mb_serial_ndx)= {unit_bom_items.inv_serial};
        wafer_table.wafer_lot(cur_mb_serial_ndx)   = {cur_wafer_lot            };
        wafer_table.wafer_number(cur_mb_serial_ndx)= {cur_wafer_number         };
        wafer_table.die_index(cur_mb_serial_ndx)   = {cur_die_index            };
    else
        error_message = sprintf('ERROR_FROM: getUnitBOMItems Empty unit_bom_items for serial %-s',char(response_table.serial(cur_mb_serial_ndx)));
        error_flag =1;
        if (display_flag)
            sprintf('\n%-s',error_message);
        end
    end
end % for each serial
end % fn get_wafer_info_for_mb_carrier

