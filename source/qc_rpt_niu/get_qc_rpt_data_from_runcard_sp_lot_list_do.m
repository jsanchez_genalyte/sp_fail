% ref: do_get_qc_rpt_data_from_runcard is a script to call: get_qc_rpt_data_from_runcard

% S1: to run: get_qc_rpt_data_from_runcard_sp_lot_list
%             fill in input paramteres with workorder and needed details: using work_order
input_params                    = {};
input_params.webservice_url     = 'http://10.0.2.226/runcard/soap?wsdl';
input_params.inv_serial         = '';                       % wafer serials are unknown, so it will pull all of the 445 parts
input_params.inv_status         = 'CONSUMED';
input_params.inv_part_number    = '00445';
input_params.inv_part_revision  = '01';
input_params.inv_work_order     = '';                       % unknown. What is known is the spotting lot number.
input_params.inv_op_code        = '';                       % jas_tbd. previous op_code ='S380'; 
input_params.inv_seq_number     = '';                       % jas_tbd.
input_params.inv_warehouse_loc  = 'Consumable Inventory';
input_params.inv_warehouse_bin  = 'INVENTORY';

input_params.qc_sp_lot_list     = { 'AB1602' };             % spot lot number list 
input_params.qc_part_bin        = '2';    
input_params.qc_status          = 'COMPLETE';
input_params.qc_part_number     = 'QCP0023-QC';
input_params.qc_part_revision   = '01';
input_params.qc_work_order      = '';                       % 'EWO-0137'; only for dbg.
input_params.qc_op_code         = 'S380';                      
input_params.qc_seq_number      = 30;
input_params.qc_warehouse_loc   = 'CLEAN ROOM: SV01.112';   % jas_tbd  or '	CLEAN ROOM: SV01.112'
input_params.qc_warehouse_bin   = 'INVENTORY';              % jas_tbd  either WIP or '2' 'INVENTORY'
input_params.display_flag       = 1;                        % 1 == display as process goes on. 0 == be quiet
% call: get_qc_rpt_data_from_runcard
[ qc_rpt_table,error_message ] = get_qc_rpt_data_from_runcard_sp_lot_list( input_params );


% S2: to run: get_qc_rpt_data_from_runcard 
%             fill in input paramteres with workorder and needed details: using work_order
input_params               = {};
input_params.webservice_url= 'http://10.0.2.226/runcard/soap?wsdl';
input_params.part_number   = 'QCP0023-QC';
input_params.work_order    = 'EWO-0137';
input_params.op_code       = 'S380';                      
input_params.seq_number    = 30;
input_params.warehouse_loc = 'CLEAN ROOM: SV01.112';    % jas_tbd  or '	CLEAN ROOM: SV01.112'
input_params.warehouse_bin = 'INVENTORY';               % jas_tbd  either WIP or '2' 'INVENTORY'
input_params.display_flag  = 1;                         % 1 == display as process goes on. 0 == be quiet
% call: get_qc_rpt_data_from_runcard

[ qc_rpt_table,error_message ] = get_qc_rpt_data_from_runcard( input_params );

% This table is the one needed to automate the qc_reports.

disp('DONE RETRIEVING QC_RPT_DATA FROM RUNCARD')




% leftovers:

[response20, error_value, error_message]   = fetchInventoryItems(obj, serial, status, part_number, part_revision...
    , work_order,op_code ,  seq_number, warehouse_loc, warehouse_bin); 

[response20, error_value, error_message]   = fetchInventoryItems(obj, serial, '', part_number, ''...
    , work_order,op_code ,  seq_number, 'PRODUCTION FLOOR', ''); 

response_table20                 = struct2table(response20);


% 
% input_params.part_number   = '00445';
% input_params.op_code       = 'A320';                      
% input_params.seq_number    = 20;
