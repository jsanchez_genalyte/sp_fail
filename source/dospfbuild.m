% script: dospfbuild
% ref:    building sp fail executable: hardcoded: run_card production or run_card_test  
close all;
clear;
clear obj;
clear all;

cache_dir                  = 'C:\Users\jsanchez\Documents\Temp\jsanchez\mcrCache9.1\';

% Check if the cfg folder exists:
if ( ( exist(cache_dir,'dir') == 7))
    % Folder Exist and it is a directory:  If dir exists: Remove all cache dirs
    cd  (cache_dir);
    % CMD for loop to recursivelly remove all dirs: To start clean and avoid compiling with the wrong runcard db.
    !for /d %G in ("dies*") do rd /s /q "%~G"
end

% WE ARE CLEAN: Ready to compile: 

cd  ('C:\Users\jsanchez\Documents\sp_fail\source\');
utils_save_all_editor_open_files;

% to compile:   old: genstore2 new: userstore
mcc     -m  sp_fail                  -o  sp_fail
%   mcc -m sp_fail  -a ..\configure  -o  sp_fail -R 'logfile','sp_fail.log'
% -a @runcard_wsdl
!copy sp_fail.exe C:\Users\jsanchez\Documents\sp_fail\exe\
!move sp_fail.exe \\userstore\Users\Storage\jsanchez\Documents\sp_fail\exe\
% !C:\Users\jsanchez\Documents\sp_fail\exe\sp_fail.exe

% ml_doc: https://www.mathworks.com/products/compiler/supported/compiler_support.html
% h = matlab.desktop.editor.getAll; h.close
% cd 'C:\Users\jsanchez\Documents\algo2\source'
% cd 'C:\Users\jsanchez\Documents\fcc\source'
% cd 'C:\Users\jsanchez\Documents\sp_fail\source'

%   ~/Documents/Temp/jsanchez/mcrCache9.1/dies1/sp_fail/@runcard_wsdl

% Original character   Escaped character
% "                    &quot;
% '                    &apos;
% <                    &lt;
% >                    &gt;
% &                    &amp;



