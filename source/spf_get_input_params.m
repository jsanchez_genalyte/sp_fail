function [ input_params_struct,error_message ] = spf_get_input_params( )
%SPF_GET_INPUT_PARAMS Retrieves input params for the sp_fail app.
%   Returns a struct with the params.
%   input files: hardcoded files: .m or .xlsm sitting in the ..\configure\ directory.

error_message                   = '';
%cfg_params_config_path         = 'W:\sp_fail\';                          % End User to be copied to ..\cfg_and_data\
%cfg_params_config_path          = 'C:\Users\jsanchez\Documents\sp_fail\configure\'; % Jas's  file for dbg

logical_str                     = {'FALSE', 'TRUE'};
sp_fail_root_dir                = 'sp_fail';         % jas_hardcoded_expected  C:\Users\jsanchez\Documents\dcrc\source\cfg\sp_fail_cfg.m
if (isdeployed)
    % COMPILED VERSION USED A .xlsm FILE FOR CONFIGURATION
    cfg_params_file_name        = 'sp_fail_configure.xlsm'; % jas_hardcoded_expected in this dir: ..\cfg\ relative to the exe
else
    % SCRIPT VERSION USED A .m FILE FOR CONFIGURATION
    cfg_params_file_name        = 'sp_fail_configure.xlsm';
    %cfg_params_file_name        = 'sp_fail_configure.m'; % jas_hardcoded_expected in this dir: ..\cfg\ relative to the source
end
cur_pwd                         = pwd;
input_params_struct             = struct;
ndx                             = strfind(cur_pwd,sp_fail_root_dir);
if ( (~(isempty(ndx)))  && (ndx > 1) )
    % FOUND MAIN DIR FOR THE APP: sp_fail
    base_dir                    = cur_pwd(1:ndx+6); %jas_hard_coded to get the base_dir: function of the length of the string: sp_fail
    cfg_dir                    = strcat(base_dir,'\configure\');
    
    % Check if the cfg folder exists:
    if ( ( exist(cfg_dir,'dir') == 7))
        % Folder Exist and it is a directory: We are almost done.
        cfg_params_full_name =strcat(cfg_dir,cfg_params_file_name);
    else
        % Folder does not exist: Prompt for loc of input excel file.
        error_message = sprintf('ERROR: sp_fail can not run due to missing: \nconfigure directory: %s\n',cfg_dir);
        return
    end
    % DS CONFIGURE DIRECTORY EXIST Try to open app: cfg file
    
    if ((isdeployed) || ( strcmp(cfg_params_full_name(end-3:end),'xlsm')))
        % COMPILED VERSION USED A .xlsm FILE FOR CONFIGURATION: read it.
        excel_tab_number            = 1;   % i.e sheet_name: sp_fail_input_parameters
        [input_params_struct,error_message] =  spf_get_excel_input_params( cfg_params_full_name,excel_tab_number);
        if ((~isempty(error_message)) && (strcmpi(error_message(1:5),'ERROR')))
            return;
        end
        if ( (~isempty(input_params_struct)) &&  ((isempty(error_message)) || (strcmp(error_message(1:7),'WARNING')) ) )
            fprintf('\n Reading CONFIG PARAMETERS ... DONE\n ');
            return
        else
            fprintf('\n Reading CONFIG PARAMETERS ... ERROR\n');
            fprintf('%s',error_message);
        end
        return;
    end; % deployed
    % SCRIPT VERSION USED A .m FILE FOR CONFIGURATION
else
    error_message = sprintf('ERROR: sp_fail APP can not run due to missing: \nroot directory: %s\nSee read_me.txt file in the doc directory\n',sp_fail_root_dir);
end % dir exist.

end % fn: read_cfg_params_file

