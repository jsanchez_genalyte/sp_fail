function  [ sp_fail_data_struct,  error_message ]  = spf_read_spf_excel_file(sp_fail_excel_file_name,rack_expected_cnt)
%SPF_READ_SPF_EXCEL_FILE   reads each tabl of the spf_file and returns a cell list with the list of failures and wo_ndx_map_table
% Each tab represents a spoting rack
% The spf_excel_file_name contains the full path to the file.
% Nick is the owner of the file, and He will periodically updates the file as needed.

% JAS_Temporal: Rather than reading the file, right now I am hardcoding the first 4 sampling plans. jas_tbd.
% Sample call:
% [ sp_fail_list_cell,  error_message ]  = spf_read_spf_excel_file('C:\Users\jsanchez\Documents\smpling_plan\configure\sampling_plan_master.xlsm');
% pipeline:    sp_fail_list_cell  --> sp_part_bin_update  -->  spf_read_spf_excel_file  -->  sp_get_excel_range
sp_fail_data_struct = struct;
fprintf(' Reading EXCEL Spot file:\t%-s   ... ',sp_fail_excel_file_name); % (sp_fail_excel_file_name)
% get the list of tabs
sp_fail_list_cell       = {};
fail_rows_list_cell     = {};
fail_cols_list_cell     = {};
wo_ndx_map_table_cell   = {};
fail_rack_list_cell     = {};
fail_alpha_list_cell     = {};
[status,sheet_list] = xlsfinfo(sp_fail_excel_file_name); % additionally returns the name of each spreadsheet in the file.
if (~(strcmp(status,'Microsoft Excel Spreadsheet')))
    % USER DID NOT SPECIFIED AN EXCEL FILE: get out
    error_message = sprintf(' Parameter wo_build_excel_file_name: %s is not a Microsoft Excel Spreadsheet',sp_fail_excel_file_name);
    return;
end
fprintf(' DONE ');

% USER DID  SPECIFIED AN EXCEL FILE: load the xlsx file passed as argument
sheet_cnt = size(sheet_list,2);
%jas_05_10_2018 addition: estimate expected number of racks (so operator does need to remeber to delete the extra excel tabs)
%                         avoid reading the extra empty excel tabs:
sheet_cnt = min(sheet_cnt ,rack_expected_cnt); % pick the lowest number of tabs

for tab_ndx = sheet_cnt:-1:1
    % Process them from last to first (so the print out comes in right order.
    cur_sheet_name = sheet_list{tab_ndx};
    if ( strfind(lower(cur_sheet_name),'rack'))
        [ sp_fail_tab_struct, error_message ] = spf_get_excel_range( sp_fail_excel_file_name,tab_ndx);
        if (~(isempty(error_message))) % || (strcmp(error_message(1:7),'WARNING'))
            error_message = sprintf('\n ERROR Reading Tab %s Failed. Tab will be ignored',cur_sheet_name);
            return;
        end
        %fprintf('\n DONE  ... reading Sampling Plans: \t   %-3d sampling plans\n',size(sp_fail_list_cell,2));
        fprintf('\n Reading Tab:\t\t\t\t%s \t\tDONE\n  ',cur_sheet_name);
        sp_fail_list_cell      = [ sp_fail_list_cell    ; sp_fail_tab_struct.sp_fail_ndx     ];
        fail_rows_list_cell    = [ fail_rows_list_cell  ; sp_fail_tab_struct.fail_rows       ];
        fail_cols_list_cell    = [ fail_cols_list_cell  ; sp_fail_tab_struct.fail_cols       ];
        fail_rack_list_cell    = [ fail_rack_list_cell  ; sp_fail_tab_struct.fail_rack       ];
        fail_alpha_list_cell   = [ fail_alpha_list_cell  ; sp_fail_tab_struct.fail_alpha     ];
        wo_ndx_map_table_cell  = [ wo_ndx_map_table_cell; sp_fail_tab_struct.wo_ndx_map_table];
    else
        fprintf('\n Ignoring Tab %s    ... DONE          ',cur_sheet_name);
    end
end % for each sheet in the excel file

sp_fail_data_struct.sp_fail_ndx      = sp_fail_list_cell;      % list of failures.
sp_fail_data_struct.fail_rows        = fail_rows_list_cell;
sp_fail_data_struct.fail_cols        = fail_cols_list_cell  ;
sp_fail_data_struct.fail_rack        = fail_rack_list_cell;
sp_fail_data_struct.fail_alpha       = fail_alpha_list_cell;

sp_fail_data_struct.wo_ndx_map_table = wo_ndx_map_table_cell; % map wo to spot ndx.

end % fn spf_read_spf_excel_file



