function  [ wo_build_table,  error_message ]  = spf_read_wo_build_excel_file(wo_build_excel_file_name)
%SPF_READ_SPF_EXCEL_FILE   reads tab 1 of the original wo bukild excel file issued by Mario
%   return  wo_ndx_map_table
% The purpose of the file is to read the wo_ndx and serials of the wo as originally built (issued)
% The spf_excel_file_name contains the full path to the file.

[ wo_build_table, error_message  ] = wo_build_get_excel_range( wo_build_excel_file_name,1);
if ((isempty(error_message)) && (~isempty(wo_build_table)))
    % EVERYTHNG OK:
    wo_build_table.Properties.VariableNames = {'wo_ndx', 'serial'};
else
    return;
end

% tbd_jas_here: perform some sanity check on the wo

try
    % MAKE THE WO_NDX column integer: So it can be used as an index (rather than char!)
    wo_build_table.wo_ndx = cell2matdouble(wo_build_table.wo_ndx);
catch
    error_message = 'Invalid data format for wo_build EXCEL file';
end
end  % spf_read_wo_build_excel_file;