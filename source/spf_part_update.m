function [ error_message ] = spf_part_update( input_params )
%SPF_PART_UPDATE  updates the part for the chips given 3 excel files:
% 1 - sp_fail_configure:  Configuration parameters
% 2 - wo_build_excel_file_name  Original build work order issued by Mario
% 3 - sp_fail_excel_file_name   fail filled during visual inspection at the spotting. It has S11 indices
%   Perform Spotting Failure log into RunCard.
% pipleine:  spf_part_update  --> spf_read_wo_build_excel_file -->
%                             --> sp_fail_excel_file_name      -->
%
error_message = '';
debug_flag = false; % version
if (debug_flag)
    input_params
end
webservice_url              = input_params.webservice_url; % 'http://10.0.2.226/runcard/soap?wsdl';          % for real
%                                             % 'http://10.0.2.226/runcard_test/soap?wsdl';    % test ONLY
wo_number                   = input_params.wo_number;
target_op_code              = input_params.target_op_code; % 'Q410'; ?Optical Test Evaluation? operation code for test step
warehouse_bin               = input_params.warehouse_bin;  % 'SCRAP';              % 'fix' Bin assignment required in RunCard
warehouse_loc               = input_params.warehouse_loc;  % 'Scrap Bin';          % 'fix' Location defined in RunCard to place scrapped chips
user_name                   = input_params.user_name;      % 'jsanchez';           % 'fix''mornelas'
trans_type                  = input_params.trans_type;     % 'Scrap';              % 'fix'
comment                     = input_params.comment;        %  'FSR Failures';       % 'fix'


createClassFromWsdl(webservice_url);
obj = runcard_wsdl;
%methods(obj)
[ wo_build_table,  error_message ]     = spf_read_wo_build_excel_file(input_params.wo_build_excel_file_name);
if ( (~(isempty(error_message)) ) ||   (isempty(wo_build_table)))  % || (strcmp(error_message(1:7),'WARNING'))
    % EITHER ERRORS OR EMPTY WO: Get out.
    return;
end
fprintf('\n Work order chip_count:\t\t%-4d   ',height(wo_build_table));
%jas_05_10_2018 addition: estimate expected number of racks (so operator does need to remeber to delete the extra excel tabs)
% jas_07_17_ corrected estimation:
%rack_expected_cnt = double(idivide(int32(242),int32(height(wo_build_table)),'ceil')); 
rack_expected_cnt = double(idivide(int32(height(wo_build_table)),(int32(242)),'ceil'));

fprintf('\n Reading WO Build file:\t\t%-s    ... DONE          \n',input_params.wo_build_excel_file_name);
% Read now the sps_fail excel file:
[ sp_fail_data_struct,  error_message ]  = spf_read_spf_excel_file(    input_params.sp_fail_excel_file_name,rack_expected_cnt);
if (~(isempty(error_message)))    % || (strcmp(error_message(1:7),'WARNING'))
    % EITHER ERRORS OR EMPTY WO: Get out.
    return;
end

if  (isempty(sp_fail_data_struct.sp_fail_ndx))  % || (strcmp(error_message(1:7),'WARNING'))
    % EMPTY SP FAILURES: Get out.
    fprintf('\n No changes made to RunCard because there were no Spot Failures   \t       ... DONE          \n');
    return;
end

% NO ERRORS AND AT LEAST ONE FAILURE:

%check the status of each serial number
fprintf('\n ');
fprintf('\n ______________________  LIST OF SPOT FAILURES  ___________________\n');
fprintf('\n Rack \tWO_Index  \t\tSpot_Row   \tSpot_Col  \t\t\tSerial       ');    % \t\tWafer_Index');
fprintf('\n ____ \t______    \t\t________   \t________  \t\t\t_______________\n'); %_\t\t___________\n');
cur_serial_ndx      = 0;
fail_total_cnt      = size(sp_fail_data_struct.sp_fail_ndx,1);
fail_chip_serials   = cell(fail_total_cnt,1);
for cur_fail_ndx    = fail_total_cnt:-1:1
    % PRINT EACH SERIA: Do it backwards to make it easy to compare with the excel file.
    cur_wafer_ndx   = sp_fail_data_struct.sp_fail_ndx(cur_fail_ndx);
    cur_wafer_ndx   = cur_wafer_ndx{1};
    ndx             = wo_build_table.wo_ndx(cur_wafer_ndx);
    serial          = wo_build_table.serial{ndx};      %
    cur_serial_ndx  = cur_serial_ndx+1;
    % save the serial to do the real job: transaction
    fail_chip_serials{cur_serial_ndx} = serial;
    fprintf('\n %-4d  \t%-s  \t\t\t%-4d \t\t%-4d  \t\t\t\t%-s  \t%-4d '...
        ,sp_fail_data_struct.fail_rack{cur_fail_ndx}...
        ,sp_fail_data_struct.fail_alpha{cur_fail_ndx}... % ,sp_fail_data_struct.sp_fail_ndx{cur_fail_ndx}...
        ,sp_fail_data_struct.fail_rows{cur_fail_ndx}...
        ,sp_fail_data_struct.fail_cols{cur_fail_ndx}...
        ,serial); % ,ndx );
    if (ndx ~= cur_wafer_ndx)
        fprintf('\n WARING Mistmatch: ndx = %-4d wafer_ndx = %-4d',ndx,cur_wafer_ndx);
    end
end % for each failure chip serial
fprintf('\n');

cur_serial_total      = cur_serial_ndx;
ok_cnt                = 0;
for cur_serial_ndx    = 1:1:cur_serial_total
    serial            = fail_chip_serials{cur_serial_ndx};
    [response, error, msg] = getUnitStatus(obj,serial);
    if error ~= 0
        error_message = sprintf (' Skipping serial %-s  \t RunCard exception encountered : %-s    \n Skipping serial %-s',serial,msg);
        fprintf('\n%-s\n',error_message);
        continue
    end
    if ~strcmp(response.opcode, input_params.target_op_code)
        error_message = sprintf (' Skipping serial %-s \t Invalid opcode %-s expecting %-s ',serial,response.opcode,target_op_code);
        fprintf('\n%-s\n',error_message);
        continue
    end
    
    if ~strcmp(response.workorder, input_params.wo_number)
        error_message = sprintf (' Skipping serial %-s  \t nIvalid workorder %-s  expecting %-s ',serial, response.workorder ,wo_number);
        fprintf('\n%-s\n',error_message);
        continue
    end
    
    if ~strcmp(response.status, 'IN QUEUE') && ~strcmp(response.status, 'IN PROGRESS')
        error_message = sprintf (' Skipping serial %-s  \t Invalid status %-s  expecting IN QUEUE  or IN PROGRESS  ',serial, response.status  );
        fprintf('\n%-s\n',error_message);
        continue
    end
    % % %
        
    transactionInfo.username        = user_name;
    transactionInfo.transaction     = trans_type;
    transactionInfo.serial          = response.serial;
    transactionInfo.workorder       = response.workorder;
    transactionInfo.seqnum          = response.seqnum;
    transactionInfo.opcode          = response.opcode;
    
    transactionInfo.warehousebin    = warehouse_bin;
    transactionInfo.warehouseloc    = warehouse_loc;
    transactionInfo.comment         = comment;
    
    transactionData                 = '';
    transactionBOM                  = '';
    
    % jas_debug: temporary until everything is debugged.
    error =0;
    msg ='';
    % jas_tbd:
    %jas_debug_07_17 comment out next line to debug the rack index: row,col is fine.
    % Alex request: add gel pack indices: row col: up to 2 gelpacks per rack index:
    %               note: indices in gel_pack are sequencial. Just read the gel_packs, find the chip
    %                     and report the 3 new columns: gp_row rp_col gp_number (1,2 the 3,4, then 4,5).
   [error, msg] = transactUnit(obj,transactionInfo, transactionData, transactionBOM); % *** TRANSACTION ***
    if error ~= 0
        error_message = sprintf (' Skipping serial %-s  RunCard exception encountered when doing a %s  Transaction: %s',serial, transactionInfo.transaction, msg   );
        fprintf('\n%-s\n',error_message);
        continue
    else
        info_message = sprintf (' OK RunCard serial %-s  \tStatus %-s  %s ',serial, response.status,msg);
        fprintf('\n%-s',info_message);   
        ok_cnt = ok_cnt +1;
    end
end % for each failure chip serial: RUNCARD transaction.
fprintf('\n');
if (ok_cnt == cur_serial_total)
    info_message = sprintf('Transactions for ALL Spot Failures   DONE\n');
    fprintf('\n%-s',info_message);
else
    info_message = sprintf('Transactions for ONLY %4d / %-4d  DONE\n',ok_cnt,cur_serial_total);
    fprintf('\n%-s',info_message);
    error_message = 'Incomplete Spot Fail request';
end

end % fn spf_part_update

