function [ wo_build_table, error_message  ] = ...
    wo_build_get_excel_range( an_excel_file_name,excel_tab_number)
%WO_BUILD_GET_EXCEL_RANGE returns a table with 2 columns: one ndx and one for serials from the original virgin issued work order from MARIO
% Input:  an_excel_file_name  Excel file name (full path) with data to be processed with the std_curve_fit app.
%         excel_tab_number    Excel tab number(BE AWARE THAT EVEN HIDEN TABS ARE PART OF EXCEL NUMBERING:
%                             Warning: Do not hide tabs in excel: It is very misleading.
% Output: a table with 2 columns: one for the list of spot fail found.
% Assumptions:
%   1 - First row is a dummy title
%   2 - the second row to the end is the real data. 
%   3 - Col_2: wo_ndx: Integers and col_3: serials.
% pipeline:  sp_fail  -->  spf_part_update --> spf_read_wo_build_excel_file --> wo_build_get_excel_range
% pipeline:  sp_fail  -->  spf_part_update --> spf_read_spf_excel_file      --> spf_get_excel_range
error_message          = '';
wo_build_table           = table();  % (sp_fail_max_cnt,1);

% read the whole sheet as a table:
sheet_table= readtable(an_excel_file_name...
    ,'ReadVariableNames',false,'FileType','spreadsheet'...
    ,'Sheet',excel_tab_number  );
try
    wo_build_table     = sheet_table(2:end,2:3); % cols 2 and 3 is what we care: wo_ndx and serials
catch
    error_message = sprintf('\nERROR: Invalid Excel file format.  \nTab 1: columns 2:3 are expected to be work order ndx and serials. Fix it and try again');
    return;
end % catch:
end % fn wo_build_get_excel_range

