% Main file: sp_fail.m
% Script to perform the spot fail given 3 excel files: sp_fail_configure, with 1 tabs: 1_ sp_fail_configure 2_ sp_fail_data
% file_1: sp_fail_configure                                                                     tab_1: sp_fail_configure.  
% file_2: wo_original:  O:\Build Orders\AB1602                                                  tab_1: sp_fail_configure.  
% file_3: sp_failures:  O:\Manufacturing\Spotter Lot History\2018 Spotter Rejects\Lot AB1602    tab_1 to tab_5: Lot AB1602 or AB1608 
% sp_fail_app:
% %       h = matlab.desktop.editor.getAll; h.close
%     cd 'C:\Users\jsanchez\Documents\sp_fail\source\'
% to compile: see dospfbuild.m
%   !copy sp_fail.exe C:\Users\jsanchez\Documents\sp_fail\exe\
%   !move sp_fail.exe \\genstore2\Users\Storage\jsanchez\Documents\sp_fail\exe\
%   ml_doc: https://www.mathworks.com/products/compiler/supported/compiler_support.html

% tbd: Friday_04_27:
% 1 - debug last set of error checking detection ONLY. 

% Define Arguments: (see end of this file)

% to test:  runcard_test: use wo: DWO-0051 

% pipeline: sp_fail --> spf_get_input_params  --> spf_get_excel_input_params (excel)  -->  spf_get_excel_range (excel)
%                   --> spf_part_update  
% Rev_history
% Rev 1.00 Date: 2018_04_17 First compiled version: (clone from the sp_fail app) 
% Rev 2.00 Date: 2018_07_04 Make robust the parsing of the excel file: If operator changes data type froms str to int: still works the parsing.
% Rev 3.00 Date: 2018_07_17 Fixed bug: estimated number of racks.

run_date = datestr(now);
fprintf('\n Initializing Spot Fail App:   Rev 3.00  Production ok Rack Count\n Run Date-time                 %-s\n',run_date);
[ input_params,error_message ] = spf_get_input_params();
if (~(isempty(error_message)))
    fprintf('\n ERROR:\n%s\n',error_message);
    fprintf('\n');
    pause(20);
    return;
end
[ error_message ]  = spf_part_update( input_params);
if ((~(isempty(error_message))) && (~(strcmpi(error_message,'Inventory part bin updated'))))
    fprintf('\nERROR:,\n%s\n',error_message);
end
fprintf('\n SPOT FAIL REQUEST DONE_________________________ Please make sure that this output matches the EXCEL Spot File\n\n'); 
fprintf('\n Report any differences found to  Jorge.Sanchez@genalyte.com \n\n'); 
% _______________________________________________________________________________________________________
if (isdeployed)
    fprintf('\n To produce a TEXT report of this output:  \n');    
    fprintf('\n    1. Copy to the clipboard all the output do: Edit_menu --> Select All --> Enter  ');
    fprintf('\n    2. Paste into an email message: CTRL-V to share the report. \n');      
    fprintf('\n To verify results: Follow these steps in RunCard:  \n');  
    fprintf('\n   1. Search for the needed WO in Terminal');  
    fprintf('\n   2. more input from Mario ... In the terminal view select  ... ');  
    fprintf('\n   3. more input from Mario ...In the View List drop-down select carrier view. ... ');  
    fprintf('\n   4. more input from Mario ...The colors of each Gelpack in carrier view should reflect the new part_bin_numbers ... ');
    fprintf('\n\n Bye ... \n');    
    % ALLOW THE USER TO DO CTRL-A CTRL-C
    pause(40); 
end
