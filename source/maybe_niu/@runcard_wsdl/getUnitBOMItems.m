function [xReturn,error,msg] = getUnitBOMItems(obj,serial,workorder,seqnum)
%getUnitBOMItems(obj,serial,workorder,seqnum)
%
%   Returns BOM components for the specified seria, work order and step.  If work order is left blank, function will search for current work order serial is associated with.  Step can be left empty as well for all BOM items registered across all steps.  
%   
%     Input:
%       serial = (string)
%       workorder = (string)
%       seqnum = (int)
%   
%     Output:
%       return{:} = (unitBOMitem)
%       error = (int)
%       msg = (string)

% Build up the argument lists.
values = { ...
   serial, ...
   workorder, ...
   seqnum, ...
   };
names = { ...
   'serial', ...
   'workorder', ...
   'seqnum', ...
   };
types = { ...
   '{http://www.w3.org/2001/XMLSchema}string', ...
   '{http://www.w3.org/2001/XMLSchema}string', ...
   '{http://www.w3.org/2001/XMLSchema}int', ...
   };

% Create the message, make the call, and convert the response into a variable.
soapMessage = createSoapMessage( ...
    'urn:runcard_wsdl', ...
    'getUnitBOMItems', ...
    values,names,types,'rpc');
response = callSoapService( ...
    obj.endpoint, ...
    'urn:runcard_wsdl#getUnitBOMItems', ...
    soapMessage);
[xReturn,error,msg] = parseSoapResponse(response);
